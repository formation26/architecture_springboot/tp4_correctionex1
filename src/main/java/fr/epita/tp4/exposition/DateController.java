package fr.epita.tp4.exposition;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp4.application.DateService;

@RestController
@RequestMapping("/dateNaissance")
public class DateController {
	
	@Autowired
	DateService service;
	
	@GetMapping("/{dateNaissance}")
	public String getDayOfWeek(@PathVariable("dateNaissance") String dateNaissance) {
		LocalDate date=LocalDate.parse(dateNaissance);
		
		return service.getDayOfWeek(date);
	}

}
