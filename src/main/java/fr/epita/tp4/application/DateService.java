package fr.epita.tp4.application;

import java.time.LocalDate;

public interface DateService {
	
	String getDayOfWeek(LocalDate date);

}
