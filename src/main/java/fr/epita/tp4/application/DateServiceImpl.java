package fr.epita.tp4.application;

import java.time.LocalDate;

import org.springframework.stereotype.Service;


@Service
public class DateServiceImpl implements DateService {

	@Override
	public String getDayOfWeek(LocalDate dateNaisance) {
		
		return dateNaisance.getDayOfWeek().toString();
	}

}
