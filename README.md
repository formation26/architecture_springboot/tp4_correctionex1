# Exercice 1 : 

---

Ecrire un programme dans lequel l’utilisateur donne sa date de naissance et le programme lui renvoie le jour de la semaine.

 Pour trouver le jour de la semaine en JAVA 
 
```java
String tempDate = "1990-06-10";      
LocalDate dateNaissance = LocalDate.parse(tempDate);      
DayOfWeek jour = dateNaissance.getDayOfWeek();      
System.out.println(jour);
```

La date est saisie via un navigateur 
<http://localhost:8080/dateNaissance/1990-06-10>